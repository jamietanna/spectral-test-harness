# Spectral Test Harness

A test harness to make it easier to write tests for your [Spectral](https://github.com/stoplightio/spectral) API linting rules.

## Install

To install it for the current project, as a development-only dependency:

```sh
npm install --save-dev @jamietanna/spectral-test-harness
```

## Usage

An example project can be found [on GitLab at jamietanna/spectral-jest](https://gitlab.com/jamietanna/spectral-jest).

This project is somewhat opinionated, and expects the following hierarchy:

```
test
├── rules.test.js
├── semver.test.js
└── testdata
    ├── complete
    │   └── valid.yaml
    └── semver
        ├── invalid.yaml
        └── valid.yaml

3 directories, 5 files
```

Then within the `semver.test.js`:

```javascript
const { retrieveDocument, setupSpectral, resultsForCode } = require('@jamietanna/spectral-test-harness')

describe('semver', () => {
  test('fails when not a number', async () => {
    const spectral = await setupSpectral('ruleset.yaml')
    const document = retrieveDocument('semver/invalid.yaml')

    const results = resultsForCode(await spectral.run(document), 'semver')

    expect(results.length).toBeGreaterThanOrEqual(1)
  })

  test('passes when valid format', async () => {
    const spectral = await setupSpectral('ruleset.yaml')
    const document = retrieveDocument('semver/valid.yaml')

    const results = resultsForCode(await spectral.run(document), 'semver')

    expect(results).toHaveLength(0)
  })
})
```

To test the following ruleset:

```yaml
extends:
  - spectral:oas
rules:
  # via https://github.com/openapi-contrib/style-guides/blob/c5326037027afb8bd0ce5a5d4ad88be7b048ef64/openapi.yml
  semver:
    severity: error
    recommended: true
    message: Please follow semantic versioning. {{value}} is not a valid version.
    given: $.info.version
    then:
      function: pattern
      functionOptions:
        match: "^([0-9]+.[0-9]+.[0-9]+)$"
```

## FAQs

### Jest `Cannot find module 'nimma/legacy'`

If you're hitting an issue like the below:

```
 FAIL  test/rules.test.js
  ● Test suite failed to run

    Cannot find module 'nimma/legacy' from '../spectral-test-harness/node_modules/@stoplight/spectral-core/dist/runner/runner.js'

    Require stack:
      /home/jamie/workspaces/jvt.me/tmp/spectral-test-harness/node_modules/@stoplight/spectral-core/dist/runner/runner.js
      /home/jamie/workspaces/jvt.me/tmp/spectral-test-harness/node_modules/@stoplight/spectral-core/dist/runner/index.js
      /home/jamie/workspaces/jvt.me/tmp/spectral-test-harness/node_modules/@stoplight/spectral-core/dist/spectral.js
      /home/jamie/workspaces/jvt.me/tmp/spectral-test-harness/node_modules/@stoplight/spectral-core/dist/index.js
      /home/jamie/workspaces/jvt.me/tmp/spectral-test-harness/index.js
      test/rules.test.js

      at Resolver.resolveModule (node_modules/jest-resolve/build/resolver.js:324:11)
      at Object.<anonymous> (../spectral-test-harness/node_modules/@stoplight/spectral-core/src/runner/runner.ts:9:1)
```

This is caused by Jest only supporting [package exports](https://nodejs.org/docs/latest-v12.x/api/packages.html#packages_exports) in version 28.0.0 or later, so if you are using an older version you will need to upgrade. 
