const { fetch } = require('@stoplight/spectral-runtime')
const { Spectral, Document } = require('@stoplight/spectral-core')
const Parsers = require('@stoplight/spectral-parsers')
const fs = require('fs')
const path = require('path')
const { bundleAndLoadRuleset } = require('@stoplight/spectral-ruleset-bundler/with-loader')
const { DiagnosticSeverity } = require('@stoplight/types')

const retrieveRuleset = async (filePath) => {
  return await bundleAndLoadRuleset(path.resolve(filePath), { fs, fetch })
}

const retrieveDocument = (filePath) => {
  const resolved = path.resolve(path.join('test/testdata', filePath))
  const body = fs.readFileSync(resolved, 'utf8')
  return new Document(body, Parsers.Yaml, filePath)
}

const setupSpectral = async (rulesetPath) => {
  const ruleset = await retrieveRuleset(rulesetPath)
  const spectral = new Spectral()
  spectral.setRuleset(ruleset)
  return spectral
}

function resultsForCode (results, code) {
  return results.filter((r) => r.code === code)
}

function resultsForSeverity (results, severity) {
  return results.filter((r) => DiagnosticSeverity[r.severity] === severity).map((r) => {
    r.severity = DiagnosticSeverity[r.severity].toUpperCase()
    return r
  })
}

function getErrors (results) {
  return resultsForSeverity(results, 'Error')
}

function getWarnings (results) {
  return resultsForSeverity(results, 'Warn')
}

function getInformativeResults (results) {
  return resultsForSeverity(results, 'Information')
}

function getHints (results) {
  return resultsForSeverity(results, 'Hint')
}

module.exports = {
  retrieveDocument,
  setupSpectral,
  resultsForCode,
  getErrors,
  getWarnings,
  getInformativeResults,
  getHints
}
